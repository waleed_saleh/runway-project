## Clone a repository:
Run the following command within command line:
git clone https://waleed_saleh@bitbucket.org/waleed_saleh/runway-project.git

To install pip env:
pip install --user pipenv

To ensure future users of the project use the same version of runway:
pipenv sync

Regarding to run shell pip environment:
pipenv shell
